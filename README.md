# MshopFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.2.

## Features

- Back-end is done with Kotlin and Spring Boot: https://gitlab.com/mersudin/mshop-back-end
- Protected routes with Auth guard
- Lazy loading 
- JSON Web Tokens (JWT) with HS256 algorithm  is used for user info manipulation
- Password is encrypted on server side
- Custom credentials validators for email and password (Reactive forms)
- Unit & Integration tests for User and Shared module
- Responsive CSS
