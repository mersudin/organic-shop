import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faCartShopping, faHome } from '@fortawesome/free-solid-svg-icons';

import { ShoppingCart } from '../shared/model/shopping-cart';
import { AuthService } from '../shared/services/auth.service';
import { ShoppingCartService } from '../shared/services/shopping-cart.service';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {

  cart: ShoppingCart;
  faHome = faHome;
  faCart = faCartShopping;

  constructor(
    public authService: AuthService, 
    private shoppingCartService: ShoppingCartService, 
    private router: Router
  ) { }

  ngOnInit(): void {
    this.shoppingCartService.cart.subscribe(
      cart => this.cart = cart
    );
    this.shoppingCartService.createCart().subscribe(
      cart =>
        this.shoppingCartService.updateCart(new ShoppingCart(cart))
    );
  }

  logout() {
    if(this.authService.isLogedIn) {
      localStorage.removeItem('token');
      this.router.navigate(['/']);
    }
  }

}
