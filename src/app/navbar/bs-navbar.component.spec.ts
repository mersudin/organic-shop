import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';
import { of } from 'rxjs';
import { StubRouterLinkDirective } from 'src/app/testing/router-link-directive-stub';

import { ShoppingCart } from '../shared/model/shopping-cart';
import { AuthService } from '../shared/services/auth.service';
import { ShoppingCartService } from '../shared/services/shopping-cart.service';
import { BsNavbarComponent } from './bs-navbar.component';

class MockAuthService {
  isLogedIn: boolean;
  currentUser: {
    name: string,
    email: string,
    password: string,
    isAdmin: boolean
  };
}

describe('BsNavbarComponent', () => {
  let component: BsNavbarComponent;
  let fixture: ComponentFixture<BsNavbarComponent>;

  let cartService: ShoppingCartService;
  let authService: AuthService;
  let mockAuthService: MockAuthService;

  let linkDes: DebugElement[];
  let routerLinks: StubRouterLinkDirective[];

  beforeEach(async () => {
    mockAuthService = {
      isLogedIn: true,
      currentUser: { name: 'Test Name', email: '', password: '', isAdmin: true }
    };

    await TestBed.configureTestingModule({
      declarations: [BsNavbarComponent, StubRouterLinkDirective],
      imports: [HttpClientTestingModule],
      providers: [
        JwtHelperService,
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        { provide: AuthService, useValue: mockAuthService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();

    fixture = TestBed.createComponent(BsNavbarComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    cartService = TestBed.inject(ShoppingCartService);
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should #totalItemsCount be 0 on init', fakeAsync(() => {
    const cart = new ShoppingCart();
    spyOn(cartService, 'createCart').and.returnValue(of(cart));

    component.ngOnInit();

    expect(cartService.createCart)
      .withContext('Spy have been called: ')
      .toHaveBeenCalled();
    expect(component.cart.totalItemsCount)
      .withContext('Cart totalitemsCount: ')
      .toBe(0);
  }));

  it('can render #totalItemsCount default value', fakeAsync(() => {
    const cart = new ShoppingCart();
    spyOn(cartService, 'createCart').and.returnValue(of(cart));
    const span = fixture.nativeElement.querySelector('span');

    component.ngOnInit();
    fixture.detectChanges();

    // TODO find out why space is default with span
    expect(span.textContent).toEqual(' 0 ');
  }));

  it('can render #totalItemsCount after adding products to the cart', fakeAsync(() => {
    const cart = new ShoppingCart();
    spyOn(cartService, 'createCart').and.returnValue(of(cart));
    const span = fixture.nativeElement.querySelector('span');

    component.ngOnInit();
    component.cart.items.push({ id: null, product: null, quantity: 2, totalPrice: 0 });
    component.cart.items.push({ id: null, product: null, quantity: 3, totalPrice: 0 });
    fixture.detectChanges();

    expect(span.textContent)
      .withContext('expected 5 total count')
      .toEqual(' 5 ');
  }));

  it('should show Login and Register if not logged in', () => {
    mockAuthService.isLogedIn = false;
    fixture.detectChanges();
    const navLink = fixture.debugElement.queryAll(By.css('.nav-link'));
    const login: HTMLElement = navLink[1].nativeElement;
    const register: HTMLElement = navLink[2].nativeElement;

    expect(login.textContent).toContain('Login');
    expect(register.textContent).toContain('Register');
  })

  it('should show user name if logged in', () => {
    const dropdownToggle = fixture.debugElement.query(By.css('.dropdown-toggle'));
    const name: HTMLElement = dropdownToggle.nativeElement;

    expect(name.textContent)
      .withContext('expected name')
      .toContain('Test');
  });

  it('should show basic dropdown menu if user is logged in as NOT admin', () => {
    authService.currentUser.isAdmin = false;
    fixture.detectChanges();

    const dropdownMenu = fixture.debugElement.queryAll(By.css('.dropdown-item'));
    const myOrders: HTMLElement = dropdownMenu[0].nativeElement;
    const logout: HTMLElement = dropdownMenu[1].nativeElement;

    expect(myOrders.textContent).toContain('My Orders');
    expect(logout.textContent).toContain('Logout');
  });

  it('should show admin dropdown menu if user is logged in as admin', () => {
    const dropdownMenu = fixture.debugElement.queryAll(By.css('.dropdown-item'));
    const myOrders: HTMLElement = dropdownMenu[0].nativeElement;
    const manageOrders: HTMLElement = dropdownMenu[1].nativeElement;
    const manageProducts: HTMLElement = dropdownMenu[2].nativeElement;
    const logout: HTMLElement = dropdownMenu[3].nativeElement;

    expect(myOrders.textContent).toContain('My Orders');
    expect(manageOrders.textContent).toContain('Manage Orders');
    expect(manageProducts.textContent).toContain('Manage Products');
    expect(logout.textContent).toContain('Logout');
  });


  // ! ROUTING
  it('should have valid all routerLink paths when logged in', () => {
    const routes = ['','shopping-cart', 'user/orders', 'admin/orders', 'admin/products']
    
    linkDes = fixture.debugElement.queryAll(By.directive(StubRouterLinkDirective));
    routerLinks = linkDes.map(de => de.injector.get(StubRouterLinkDirective));

    expect(routerLinks.length).toBe(5);
    expect(routerLinks.every((link) => routes.includes(link.linkParams))).toBeTrue();
  })

  it('should have valid all routerLink paths when NOT logged in', () => {
    const routes = ['','shopping-cart', 'user/login', 'user/register'];
    mockAuthService.isLogedIn = false;
    fixture.detectChanges();

    linkDes = fixture.debugElement.queryAll(By.directive(StubRouterLinkDirective));
    routerLinks = linkDes.map(de => de.injector.get(StubRouterLinkDirective));

    expect(routerLinks.every((link) => routes.includes(link.linkParams))).toBeTrue();
  })

  it('can get all 5 RouterLinks from template if logged in', () => {
    // const rlinks = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    linkDes = fixture.debugElement.queryAll(By.directive(StubRouterLinkDirective));
    routerLinks = linkDes.map(de => de.injector.get(StubRouterLinkDirective));

    expect(routerLinks.length)
      .withContext('should have 5 routerLinks')
      .toBe(5);
    expect(routerLinks[0].linkParams)
      .withContext('link #1')
      .toBe('');
    expect(routerLinks[1].linkParams)
      .withContext('link #2')
      .toBe('shopping-cart');
    expect(routerLinks[2].linkParams)
      .withContext('link #3')
      .toBe('user/orders');
    expect(routerLinks[3].linkParams)
      .withContext('link #4')
      .toBe('admin/orders');
    expect(routerLinks[4].linkParams)
      .withContext('link #5')
      .toBe('admin/products');
  });

  it('can get all 4 RouterLinks from template if NOT logged in', () => {
    mockAuthService.isLogedIn = false;
    fixture.detectChanges();

    linkDes = fixture.debugElement.queryAll(By.directive(StubRouterLinkDirective));
    routerLinks = linkDes.map(de => de.injector.get(StubRouterLinkDirective));

    expect(routerLinks.length)
      .withContext('should have 4 routerLinks')
      .toBe(4);
    expect(routerLinks[0].linkParams)
      .withContext('link #1')
      .toBe('');
    expect(routerLinks[1].linkParams)
      .withContext('link #2')
      .toBe('shopping-cart');
    expect(routerLinks[2].linkParams)
      .withContext('link #3')
      .toBe('user/login');
    expect(routerLinks[3].linkParams)
      .withContext('link #4')
      .toBe('user/register');
  });

  it('can click shopping cart link in template', () => {
    linkDes = fixture.debugElement.queryAll(By.directive(StubRouterLinkDirective));
    routerLinks = linkDes.map(de => de.injector.get(StubRouterLinkDirective));
    const loginLinkDe = linkDes[1];
    const loginLink = routerLinks[1];

    expect(loginLink.navigatedTo)
      .withContext('should not have navigated yet')
      .toBeNull();

    loginLinkDe.triggerEventHandler('click');
    fixture.detectChanges();

    expect(loginLink.navigatedTo).toBe('shopping-cart');
  });

  it('should successfuly log out when token is present in local storage', () => {
    localStorage.setItem('token', 'some_valid_token');
    expect(localStorage.getItem('token')).not.toBeNull();

    component.logout();

    expect(localStorage.getItem('token')).toBeNull();
  })

});
