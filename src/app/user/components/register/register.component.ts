import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../../shared/services/auth.service';
import { EmailValidator, passwordValidator } from './../login/credentials.validators';


const unamePattern = '^[a-zA-Z0-9]+$';
const emailPatern = '[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$';
const pwdPattern = '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}';


@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  form: FormGroup;

  constructor(
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private emailValidator: EmailValidator,
  ) {
    this.form = fb.group({
      name: ['', [Validators.required, Validators.pattern(unamePattern)]],
      email: ['', {
        validators: [Validators.required, Validators.pattern(emailPatern)],
        asyncValidators: [this.emailValidator.validate.bind(this.emailValidator)],
        updateOn: 'blur'
      }],
      password: ['', [Validators.required, Validators.pattern(pwdPattern)]],
      confirmPassword: ['', Validators.required],
      isAdmin: [false]
    }, {
      validators: passwordValidator
    })
  }

  get name() { return this.form.get('name'); }
  get email() { return this.form.get('email'); }
  get password() { return this.form.get('password'); }
  get confirmPassword() { return this.form.get('confirmPassword'); }

  register() {
    this.auth.register(this.form.value).subscribe( () => this.router.navigate(['/user/login']) );
  }

}
