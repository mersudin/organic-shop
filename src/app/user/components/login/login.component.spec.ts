import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router, RouterLinkWithHref } from '@angular/router';
import { JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';
import { of, throwError } from 'rxjs';
import { ActivatedRouteStub } from 'src/app/testing/activated-route-stub';

import { AuthService } from '../../../shared/services/auth.service';
import { LoginComponent } from './login.component';


class RouterStub {

  navigate(params) { return }
}


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;
  let activatedRoute: ActivatedRouteStub;
  let router: Router;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        JwtHelperService
      ]
    })
    .compileComponents();

    const store = {};
    spyOn(localStorage, 'getItem').and.callFake( (key:string):string => {
     return store[key] || null;
    });
    spyOn(localStorage, 'setItem').and.callFake((key:string, value:string):string =>  {
      return store[key] = <string>value;
    });

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    activatedRoute = fixture.debugElement.injector.get(ActivatedRoute) as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should be invalid when empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let errors = {};

    errors = component.email.errors || {};
    expect(errors['required']).toBeTruthy();

    component.email.setValue("test");
    errors = component.email.errors || {};
    expect(errors['required']).toBeFalsy();

    expect(component.form.valid)
      .withContext('should be invalid without password')
      .toBeFalsy();
  });

  it('password field validity', () => {
    let errors = {};

    errors = component.password.errors || {};
    expect(errors['required']).toBeTruthy();

    component.password.setValue("123456");
    errors = component.password.errors || {};
    expect(errors['required']).toBeFalsy();

    expect(component.form.valid)
      .withContext('should be invalid without email')
      .toBeFalsy();
  });

  it('form validity', () => {
    component.email.setValue("test");
    component.password.setValue("123456");

    expect(component.form.valid).toBeTruthy();
  });

  it('should succeed login', () => {
    component.email.setValue("test");
    component.password.setValue("123456");
    spyOn(authService, 'login').and.returnValue(of("valid token"));

    component.login();

    expect(component.form.errors).toBeFalsy();
  });

  it('should store token in local storage after successful login', () => {
    spyOn(authService, 'login').and.returnValue(of("valid token"));
    expect(localStorage.getItem('token')).toBeNull()

    component.login();
    
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(localStorage.getItem('token')).not.toBeNull()
  });

  it('should fail to login', () => {
    component.email.setValue("test");
    component.password.setValue("123456");
    spyOn(authService, 'login').and.returnValue(throwError(() => new Error('ivalid login')));

    component.login();

    expect(component.form.errors)
      .withContext('form should have errors')
      .toBeTruthy();
    expect(component.form.errors['invalidLogin'])
      .withContext('should have "invalidLogin" error')
      .toBeTrue();
  });

  // ! CSS

  it('should higlight login button if form is valid', () => {
    const de = fixture.debugElement.query(By.css('.signinbtn'));
    expect(de.classes['validbtn'])
      .withContext('empty form')
      .toBeFalsy();

    component.email.setValue("test");
    component.password.setValue("123456");
    fixture.detectChanges();
    expect(de.classes['validbtn'])
      .withContext('filled form')
      .toBeTruthy();
  })

  it('login button should be disabled if form is invalid', () => {
    const btn = fixture.nativeElement.querySelector('button');
    expect(btn.disabled)
      .withContext('empty form')
      .toBeTruthy();

    component.email.setValue("test");
    fixture.detectChanges();
    expect(btn.disabled)
      .withContext('missing password')
      .toBeTruthy();

    component.password.setValue("123456");
    component.email.reset();
    fixture.detectChanges();
    expect(btn.disabled)
      .withContext('missing email')
      .toBeTruthy();

    component.email.setValue("test");
    fixture.detectChanges();
    expect(btn.disabled)
      .withContext('filled form')
      .toBeFalsy();
  })


  // ! ROUTING
  it('should have all valid links in template', () => {
    const routes = ['/user/register']

    const routerLinks = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));

    expect(routerLinks.every((link) => routes.includes(link.attributes['routerLink']))).toBeTrue();
  })

  it('should redirect after success login to queryParamMap', () => {
    activatedRoute.setParamMap({ returnUrl: 'check-out2' });
    spyOn(authService, 'login').and.returnValue(of("valid token"));
    const routerSpy = spyOn(router, 'navigate');
    
    component.login();

    expect(routerSpy).toHaveBeenCalledWith(['check-out2']);
  });

  it('should not redirect after failed login to queryParamMap', () => {
    activatedRoute.setParamMap({ returnUrl: 'check-out2' });
    spyOn(authService, 'login').and.returnValue(throwError(() => new Error('ivalid login')));
    const routerSpy = spyOn(router, 'navigate');

    component.login();

    expect(routerSpy).not.toHaveBeenCalledWith(['check-out2']);
  });


});
