import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('fade',[
      transition(':enter', [
        style({ opacity: 0 }),
        animate('1s', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('1s', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LoginComponent {

  form: FormGroup;

  constructor(
    public auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    fb: FormBuilder
  ) {
    this.form = fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  login() {
    this.auth.login(this.form.value).subscribe({
      next: (token) => {
        localStorage.setItem('token', token);
        this.route.queryParamMap.subscribe(
          paramsMap => this.router.navigate([paramsMap.get('returnUrl') || '/'])
        )
      },
      error: () => {
        this.form.setErrors({
          invalidLogin: true
        })
      }
    });
  }

}
