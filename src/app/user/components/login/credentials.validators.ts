import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map } from 'rxjs/operators';

import { AuthService } from '../../../shared/services/auth.service';

@Injectable({ providedIn: 'root' })
export class EmailValidator implements AsyncValidator {

  constructor(private auth: AuthService) { }

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.auth.emailExist(control.value).pipe(
      map(isTaken => isTaken ? { emailTaken: true } : null),
      catchError(() => of(null))
    );
  }

}


export const passwordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');

  return password.value !== confirmPassword.value ? { passwordsDontMatch: true } : null;
};