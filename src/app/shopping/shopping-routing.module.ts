import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../shared/services/auth-guard.service';
import { CheckOutComponent } from './components/check-out/check-out.component';
import { OrderSuccessComponent } from './components/order-success/order-success.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';

const routes: Routes = [
  { 
    path: '', component: ShoppingCartComponent 
  },
  { 
    path: 'check-out', component: CheckOutComponent, canActivate: [AuthGuard] 
  },
  { 
    path: 'order-success/:id', component: OrderSuccessComponent, canActivate: [AuthGuard] 
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShoppingRoutingModule {}