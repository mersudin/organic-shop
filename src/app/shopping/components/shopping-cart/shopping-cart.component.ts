import { Component, OnInit } from '@angular/core';

import { ShoppingCart } from '../../../shared/model/shopping-cart';
import { ShoppingCartService } from '../../../shared/services/shopping-cart.service';

@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  cart: ShoppingCart;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit(): void {
    this.shoppingCartService.cart.subscribe(
      cart => this.cart = cart
    )
  }

  // TODO
  clearCart() {
    this.shoppingCartService.clearCart().subscribe(() => {
      delete this.cart.items;
      this.cart.items = [];
      this.shoppingCartService.updateCart(this.cart);
    });
  }

}
