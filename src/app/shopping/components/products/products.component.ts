import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Product } from '../../../shared/model/product';
import { ShoppingCart } from '../../../shared/model/shopping-cart';
import { ProductService } from '../../../shared/services/product.service';
import { ShoppingCartService } from '../../../shared/services/shopping-cart.service';

const DEFAULT_SORTING_FIELD = 'id';
const PRODUCTS_PER_PAGE = 8;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  cart$: Observable<ShoppingCart>;
  products: Product[] = [];
  filteredProducts: Product[] = [];
  category: string;
  totalElements = 0;
  pageNumber = 0;
  isLoadedByCategory = false;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private shoppingCartService: ShoppingCartService
  ) { }

  ngOnInit() {
    this.cart$ = this.shoppingCartService.cart;
    this.populateProducts();
  }

  private populateProducts() {
    this.route.queryParamMap.subscribe(
      params => {
        this.category = params.get('category');
        if (this.category)
          this.loadByCategory();
        else
          this.loadAll();
      })
  }

  private loadByCategory() {
    this.productService.getByCategory(this.category).subscribe(
      products => {
        this.filteredProducts = products;
        this.isLoadedByCategory = true;
      }
    )
  }

  private loadAll() {
    this.filteredProducts = this.products;
    this.isLoadedByCategory = false;

    if (!this.products.length) {
      this.productService.getPageWithSort(this.pageNumber, PRODUCTS_PER_PAGE, DEFAULT_SORTING_FIELD).subscribe(
        page => {
          this.filteredProducts = this.products = page.content;
          this.totalElements = page.totalElements;
        }
      );
    }
  }

  loadMore() {
    this.productService.getPageWithSort(this.pageNumber + 1, PRODUCTS_PER_PAGE, DEFAULT_SORTING_FIELD).subscribe(
      page => {
        if (page.content.length) {
          this.products.push(...page.content);
          this.filteredProducts = this.products;
          ++this.pageNumber;
        }
      }
    )
  }

}
