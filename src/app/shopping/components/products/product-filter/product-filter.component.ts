import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { CategoryService } from '../../../../shared/services/category.service';
import { Category } from './../../../../shared/model/category';

@Component({
  selector: 'product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.css']
})
export class ProductFilterComponent{

  categories$ : Observable<Category[]>
  @Input() activeCategory: string;

  constructor(private categoryService: CategoryService) {
    this.categories$ = categoryService.getAll();
   }

}
