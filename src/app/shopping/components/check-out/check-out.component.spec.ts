import { FormsModule } from '@angular/forms';
import { ShippingFormComponent } from './../shipping-form/shipping-form.component';
import { ShoppingCartSummaryComponent } from './../shopping-cart-summary/shopping-cart-summary.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckOutComponent } from './check-out.component';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';

describe('CheckOutComponent', () => {
  let component: CheckOutComponent;
  let fixture: ComponentFixture<CheckOutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CheckOutComponent, ShoppingCartSummaryComponent, ShippingFormComponent],
      imports: [HttpClientTestingModule, FormsModule],
      providers: [
        JwtHelperService,
        { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CheckOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
