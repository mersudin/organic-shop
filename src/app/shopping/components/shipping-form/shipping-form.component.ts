import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Address } from '../../../shared/model/address';
import { Order } from '../../../shared/model/order';
import { ShoppingCart } from '../../../shared/model/shopping-cart';
import { AuthService } from '../../../shared/services/auth.service';
import { OrderService } from '../../../shared/services/order.service';

@Component({
  selector: 'shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit {

  @Input() cart: ShoppingCart;
  shipping: Address;
  private userId: number;

  constructor(
    private orderService: OrderService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.shipping = new Address();
    this.userId = this.authService.currentUser ? this.authService.currentUser.id : 0;
  }

  placeOrder() {
    if(this.cart){
      const order = new Order(this.userId, this.shipping, this.cart)
      this.orderService.placeOrder(order).subscribe( 
        result => this.router.navigate(['shopping-cart/order-success', result.id])
      );
    }
  }

}
