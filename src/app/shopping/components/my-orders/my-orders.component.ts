import { Observable } from 'rxjs';
import { Component } from '@angular/core';

import { Order } from '../../../shared/model/order';
import { AuthService } from '../../../shared/services/auth.service';
import { OrderService } from '../../../shared/services/order.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent {

  orders$: Observable<Order[]>

  constructor(private authService: AuthService, private orderService: OrderService) {
    if(this.authService.currentUser){
      this.orders$ = this.orderService.getOrdersByUser(this.authService.currentUser.id);
    }
   }

}
