import { convertToParamMap, ParamMap, Params } from '@angular/router';
import { ReplaySubject } from 'rxjs';

export class ActivatedRouteStub {
  private subject = new ReplaySubject<ParamMap>();

  // constructor(initialParams?: Params) {
  //   this.setParamMap(initialParams);
  // }

  readonly queryParamMap = this.subject.asObservable();

  // readonly snapshot = {
  //   queryParamMap: {
  //     get(p: string): string | null { 
  //       return this.queryParamMap[p]; 
  //     }
  //   }
  // }

  setParamMap(params: Params = {}) {
    this.subject.next(convertToParamMap(params));
  }
}