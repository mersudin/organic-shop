import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminOrdersComponent } from './components/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { AdminAuthGuard } from './services/admin-auth-guard.service';

const routes: Routes = [
  {
    path: 'products/new',
    component: ProductFormComponent,
    canActivate: [AdminAuthGuard]
  },
  {
    path: 'products/:id',
    component: ProductFormComponent,
    canActivate: [AdminAuthGuard]
  },
  {
    path: 'products',
    component: AdminProductsComponent,
    canActivate: [AdminAuthGuard]
  },
  {
    path: 'orders',
    component: AdminOrdersComponent,
    canActivate: [AdminAuthGuard]
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }