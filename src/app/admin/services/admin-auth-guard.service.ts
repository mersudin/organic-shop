import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from '../../shared/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate() {
    if (this.auth.isLogedIn && this.auth.currentUser.isAdmin) {
      return true;
    }
    this.router.navigate(['/user/login']);
    
    return false;
  }
}
