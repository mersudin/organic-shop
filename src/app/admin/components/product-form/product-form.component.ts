import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Category } from 'src/app/shared/model/category';

import { Product } from '../../../shared/model/product';
import { CategoryService } from '../../../shared/services/category.service';
import { ProductService } from '../../../shared/services/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  categories$: Observable<Category[]>
  product: Product;
  id: number;

  constructor(
    private categoryService: CategoryService,
    private productService: ProductService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.categories$ = this.categoryService.getAll();
    this.id = +this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.productService.get(this.id).pipe(take(1)).subscribe({
        next: (p) => this.product = p,
        error: (e) => this.handleError(e)
      })
    } else
      this.product = { title: null, price: null, category: null, imageUrl: null };
  }

  save() {
    if (this.id) {
      this.productService.update(this.product).subscribe({
        next: () => this.showToast(true),
        error: (e) => this.handleError(e),
        complete: () => this.router.navigate(['/admin/products'])
      });
    }
    else {
      this.productService.create(this.product).subscribe({
        next: () => this.showToast(true),
        error: (e) => this.handleError(e),
        complete: () => this.router.navigate(['/admin/products'])
      });
    }
  }

  delete() {
    if (confirm('Are you sure you want to delete this product?')) {
      this.productService.delete(this.id).subscribe({
        next: () => this.showToast(true),
        error: (e) => this.handleError(e),
        complete: () => this.router.navigate(['/admin/products'])
      });
    }
  }

  private showToast(success: boolean) {
    if (success)
      this.toastr.success('Success');
    else
      this.toastr.error('Failed');
  }

  private handleError(error: any) {
    this.showToast(false);
    console.error('Error: ' + new JsonPipe().transform(error));
    this.router.navigate(['/admin/products'])
  }

}
