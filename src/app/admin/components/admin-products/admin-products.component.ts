import { Component, OnInit } from '@angular/core';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';

import { Product } from '../../../shared/model/product';
import { ProductService } from '../../../shared/services/product.service';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit {

  products: Product[] = [];
  filteredProducts: Product[] = [];
  numberOfLoadedProducts = 8;
  productsPerPage = 8;
  page = 0;

  titleSortUp = true;
  priceSortUp = true;
  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.getAllSortedBy('title').subscribe(
      products => {
        this.products = products;
        this.updateProductsPerPage();
      }
    );
  }

  filter(query: string) {
    if (query.length > 1) {
      this.filteredProducts = this.products.filter(p => p.title.toLowerCase().includes(query.toLowerCase()));
      this.numberOfLoadedProducts = this.filteredProducts.length;
    }
    else if (!query.length)
      this.updateProductsPerPage();
  }

  sortByTitle() {
    this.products = this.products.sort((a, b) => {
      if (a.title.charAt(0).toLowerCase() < b.title.charAt(0).toLowerCase()) {
        return this.titleSortUp ? 1 : -1;
      } else {
        return this.titleSortUp ? -1 : 1;
      }
    });
    this.titleSortUp = !this.titleSortUp;
    this.updateProductsPerPage();
  }

  sortByPrice() {
    this.products = this.products.sort((a, b) => this.priceSortUp ? b.price - a.price : a.price - b.price);
    this.priceSortUp = !this.priceSortUp;
    this.updateProductsPerPage();
  }

  leftButton() {
    if (this.page) {
      --this.page;
      this.updateProductsPerPage();
    }
  }

  rightButton() {
    if (this.filteredProducts.length == this.productsPerPage) {
      ++this.page;
      this.updateProductsPerPage();
    }
  }

  private updateProductsPerPage() {
    this.filteredProducts = this.products.slice(this.page * this.productsPerPage, this.page * this.productsPerPage + this.productsPerPage);
    this.countProducts();
  }

  private countProducts() {
    const count = (this.page + 1) * this.productsPerPage
    this.numberOfLoadedProducts = count < this.products.length ? count : count - (count - this.products.length);
  }

}
