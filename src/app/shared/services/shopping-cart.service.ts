import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';

import * as glob from '../../global-vars';
import { Product } from '../model/product';
import { ShoppingCart } from '../model/shopping-cart';
import { ShoppingCartItem } from '../model/shopping-cart-item';


@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  private cartSource = new BehaviorSubject<any>({});
  cart = this.cartSource.asObservable();

  constructor(private http: HttpClient) { }

  updateCart(cart: any) {
    this.cartSource.next(cart);
  }

  createCart() {
    if (this.getCartId()) return this.getCart();

    return this.http.post<ShoppingCart>(`${glob.apiURL}/shopping-carts`, {}).pipe(map(
      result => {
        localStorage.setItem('cartId', result.id.toString())
        const newCart = new ShoppingCart(result);
        this.updateCart(newCart);
        return newCart;
      }
    ))
  }

  getCart(): Observable<ShoppingCart> {
    return this.http.get<ShoppingCart>(`${glob.apiURL}/shopping-carts/` + this.getCartId());
  }

  getCartId(): number {
    return +localStorage.getItem('cartId');
  }

  addToCart(productId: number) {
    return this.http.post<ShoppingCartItem>(`${glob.apiURL}/items/cart/${this.getCartId()}/product/${productId}`, {});
  }

  removeFromCart(product: Product) {
    return this.http.delete(`${glob.apiURL}/items/cart/${this.getCartId()}/product/${product.id}`);
  }

  clearCart() {
    return this.http.delete<ShoppingCart>(`${glob.apiURL}/shopping-carts/` + this.getCartId());
  }

  deleteCart() {
    this.clearCart();
    localStorage.removeItem('cartId');
    this.createCart().subscribe();
  }

  getItems(): Observable<ShoppingCartItem[]> {
    return this.http.get<ShoppingCartItem[]>(`${glob.apiURL}/items/cart/${this.getCartId()}`);
  }

}
