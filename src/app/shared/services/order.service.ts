import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import * as glob from '../../global-vars';
import { Order } from '../model/order';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient, private shoppingCartService: ShoppingCartService) { }

  placeOrder(order: Order) : Observable<Order> {
    this.shoppingCartService.deleteCart();
    return this.http.post<Order>(`${glob.apiURL}/orders`, order)
  }

  getOrders() { 
    return this.http.get<Order[]>(`${glob.apiURL}/orders`)
  }

  getOrdersByUser(userId: number) {
    return this.http.get<Order[]>(`${glob.apiURL}/orders/user/${userId}`)
  }
}
