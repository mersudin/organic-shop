import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import * as glob from '../../global-vars';
import { Credentials } from '../model/credentials';
import { AppUser } from './../model/app-user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    public jwtHelper: JwtHelperService
  ) { }

  get currentUser(): AppUser {
    return this.jwtHelper.decodeToken();
  }

  get isLogedIn(): boolean {
    return !this.jwtHelper.isTokenExpired();
  }

  login(credentials: Credentials) {
    return this.http.post(`${glob.apiURL}/users/login`, credentials, { responseType: 'text' });
  }

  register(user: AppUser) {
    return this.http.post(`${glob.apiURL}/users/register`, user, { responseType: 'text' });
  }

  emailExist(email: string) {
    return this.http.post(`${glob.apiURL}/users/email`, email);
  }

}
