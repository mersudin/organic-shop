import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import * as glob from '../../global-vars';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  create(product: Product) : Observable<Product>  {
    return this.http.post<Product>(`${glob.apiURL}/products`,product);
  }
  
  get(productId: number) : Observable<Product> {
    return this.http.get<Product>(`${glob.apiURL}/products/` + productId);
  }

  getPageWithSort(page: number, pageSize: number, field: string) {
    return this.http.get<any>(`${glob.apiURL}/products/page/${page}/page-size/${pageSize}/field/${field}`);
  }

  getByCategory(name: string) {
    return this.http.get<Product[]>(`${glob.apiURL}/products/category/${name}`);
  }

  getAll() : Observable<Product[]> {
    return this.http.get<Product[]>(`${glob.apiURL}/products`);
  }

  getAllSortedBy(field: string) : Observable<Product[]> {
    return this.http.get<Product[]>(`${glob.apiURL}/products/sort/${field}`);
  }

  update(product: Product) : Observable<Product> {
    return this.http.patch<Product>(`${glob.apiURL}/products/`,product);
  }

  delete(productId: number) {
    return this.http.delete<Product>(`${glob.apiURL}/products/${productId}`);
  }

}
