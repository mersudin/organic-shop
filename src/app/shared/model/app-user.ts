export interface AppUser {
    id?: number;
    name: string;
    password: string;
    email: string;
    isAdmin: boolean;
}