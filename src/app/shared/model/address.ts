
export class Address {
    id?: number;
    name: string;
    addressLine1: string;
    addressLine2: string;
    city: string
}
