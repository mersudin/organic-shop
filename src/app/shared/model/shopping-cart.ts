import { Product } from './product';
import { ShoppingCartItem } from './shopping-cart-item';

export class ShoppingCart {
  id?: number;
  dateCreated: number;
  items: ShoppingCartItem[] = [];

  constructor(other?: ShoppingCart) {
    if(other) {
      this.id = other.id;
      this.dateCreated = other.dateCreated;
  
      for (const i in other.items) {
          const item = other.items[i];
          this.items.push(new ShoppingCartItem(item.id, item.product, item.quantity));
      }
    } else {
      this.id = 0;
      this.dateCreated = 0;
      this.items = [];
    }
  }

  getQuantity(productId: number) : number {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].product.id === productId){
        return this.items[i].quantity;
      }
    }
    return 0;
  }

  updateQuantity(product: Product, quantity: number, itemId?: number) : number {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i].product.id === product.id){
        this.items[i].quantity += quantity;
        if (this.items[i].quantity === 0) {
            this.items.splice(i,1);
            return 0;
        }
        return this.items[i].quantity;
      }
    }
    this.items.push(new ShoppingCartItem(itemId, product, quantity))
    return 1;
  }

  get totalItemsCount() : number {
    let count = 0;
    for (const i in this.items) {
        count += this.items[i].quantity;
    }
    return count;
  }

  get totalPrice() : number {
    let sum = 0;
    for (let i = 0; i < this.items.length; i++) {
        sum += this.items[i].totalPrice;
    }
    return sum;
  }
}
