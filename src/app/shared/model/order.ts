import { Address } from './address';
import { ShoppingCart } from './shopping-cart';

export class Order {
  id?: number;
  datePlaced: string;

  constructor(public userId: number, public shippingAddress: Address, public shoppingCart: ShoppingCart) {
      this.datePlaced = new Date().toLocaleDateString('en-CA');
  }

}
