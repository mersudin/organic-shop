import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { EMPTY, of } from 'rxjs';

import { ShoppingCart } from '../../model/shopping-cart';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { Product } from './../../model/product';
import { ProductQuantityComponent } from './../product-quantity/product-quantity.component';
import { ProductCardComponent } from './product-card.component';

class MockCartService {
  addToCart() { return of(EMPTY) }
}

describe('ProductCardComponent', () => {
  let component: ProductCardComponent;
  let fixture: ComponentFixture<ProductCardComponent>;
  let mockProduct: Product;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductCardComponent, ProductQuantityComponent],
      imports: [HttpClientTestingModule],
      providers: [{ provide: ShoppingCartService, useClass: MockCartService }]
    })
      .compileComponents();

    mockProduct = {
      id: 1,
      title: 'test',
      price: 1,
      category: 'test',
      imageUrl: 'test'
    }

    fixture = TestBed.createComponent(ProductCardComponent);
    component = fixture.componentInstance;
    component.shoppingCart = new ShoppingCart();
    component.product = mockProduct;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('default quantity should be 0', () => {
    expect(component.quantity).toBe(0);
  });


  it('should render product info in template', () => {
    const img = fixture.nativeElement.querySelector('img');
    expect(img.getAttribute('src'))
      .withContext('src attribute')
      .toEqual(mockProduct.imageUrl);

    const title = fixture.debugElement.query(By.css('.card-title')).nativeElement;
    expect(title.textContent)
      .withContext('title')
      .toEqual(mockProduct.title);

    const price = fixture.nativeElement.querySelector('p');
    expect(price.textContent)
      .withContext('price')
      .toContain(mockProduct.price);
  });

  it('should not render product if product is empty or null', () => {
    component.product.title = '';
    fixture.detectChanges();
    let card = fixture.debugElement.query(By.css('.card'));
    expect(card)
      .withContext('empty title')
      .toBeNull();

    component.product = null;
    fixture.detectChanges();
    card = fixture.debugElement.query(By.css('.card'));
    expect(card)
      .withContext('product null')
      .toBeNull();
  });


  it('should render footer when #showActions = true', () => {
    const footer = fixture.debugElement.query(By.css('.card-footer'));
    const btn = fixture.nativeElement.querySelector('button');

    expect(footer).not.toBeNull();
    expect(btn)
      .withContext('button')
      .not.toBeNull();
    expect(btn.textContent)
      .withContext('button text')
      .toContain('Add');
  });

  it('should not render footer when #showActions = false', () => {
    component.showActions = false;
    fixture.detectChanges();
    const footer = fixture.debugElement.query(By.css('.card-footer'));
    const btn = fixture.nativeElement.querySelector('button');

    expect(footer)
      .withContext('footer')
      .toBeNull();
    expect(btn)
      .withContext('button')
      .toBeNull();
  });


  it('should add 1 when called #addToCart', () => {
    component.addToCart();

    expect(component.quantity).toBe(1);
  });

  it('should add 1 when clicked #addToCart', () => {
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();

    expect(component.quantity).toBe(1);
  });


  it('should not render #product-quantity on init', () => {
    const temp = fixture.nativeElement.querySelector('product-quantity');

    expect(temp).toBeNull();
  });

  it('should render #product-quantity when clicked #addToCart', () => {
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.detectChanges();

    const temp = fixture.nativeElement.querySelector('product-quantity');

    expect(temp).not.toBeNull();
  });


});
