import { Component, Input } from '@angular/core';

import { Product } from '../../model/product';
import { ShoppingCart } from '../../model/shopping-cart';
import { ShoppingCartService } from '../../services/shopping-cart.service';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent {

  @Input() product: Product;
  @Input() showActions = true;
  @Input() shoppingCart: ShoppingCart;

  constructor(private cartService: ShoppingCartService) { }

  get quantity() {
    return this.shoppingCart ? this.shoppingCart.getQuantity(this.product.id) : 0;
  }

  addToCart() {
    this.cartService.addToCart(this.product.id).subscribe(
      result => {
        this.shoppingCart.updateQuantity(this.product, 1, result.id)
      }
    )
  }

}
