import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { EMPTY, of } from 'rxjs';

import { ShoppingCart } from '../../model/shopping-cart';
import { ShoppingCartService } from './../../services/shopping-cart.service';
import { ProductQuantityComponent } from './product-quantity.component';

describe('ProductQuantityComponent', () => {
  let component: ProductQuantityComponent;
  let fixture: ComponentFixture<ProductQuantityComponent>;
  const mockCartService = jasmine.createSpyObj('ShoppingCartService', ['addToCart', 'removeFromCart']);
  let addToCartSpy: any;
  const mockProduct = {
    id: 1,
    title: 'test',
    price: 1,
    category: 'test',
    imageUrl: 'test'
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductQuantityComponent],
      imports: [HttpClientTestingModule],
      providers: [{ provide: ShoppingCartService, useValue: mockCartService }]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ProductQuantityComponent);
    component = fixture.componentInstance;
    component.shoppingCart = new ShoppingCart();
    component.product = mockProduct;
    addToCartSpy = mockCartService.addToCart.and.returnValue(of(EMPTY));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('default quantity should be 0', () => {
    expect(component.quantity).toBe(0);
  });

  it('should update quantity when #addToCart is called', () => {
    component.addToCart();

    expect(addToCartSpy)
      .withContext('spy should have been called')
      .toHaveBeenCalled();
    expect(component.quantity).toBe(1);
  });

  it('should update quantity when #removeFromCart is called', () => {
    const removeFromCartSpy = mockCartService.removeFromCart.and.returnValue(of(EMPTY));

    component.addToCart();
    component.addToCart();
    component.addToCart();

    expect(component.quantity)
      .withContext('#addToCart called 3 times')
      .toBe(3)

    component.removeFromCart();

    expect(removeFromCartSpy)
      .withContext('spy should have been called once')
      .toHaveBeenCalledOnceWith(mockProduct);
    expect(component.quantity).toBe(2);
  });


  it('should not render quantity in template if quantity is 0', () => {
    const quantity = fixture.debugElement.query(By.css('.btn-quantity'));

    expect(quantity).toBeNull();
  })

  it('should render quantity in template if quantity > 0', () => {
    component.addToCart();
    component.addToCart();
    component.addToCart();
    fixture.detectChanges();
    
    const quantity = fixture.debugElement.query(By.css('.btn-quantity'));

    expect(quantity).not.toBeNull();
    expect(quantity.nativeElement.textContent).toContain('3');
  })

});
