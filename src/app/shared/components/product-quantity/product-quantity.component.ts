import { Component, Input } from '@angular/core';

import { ShoppingCart } from '../../model/shopping-cart';
import { Product } from '../../model/product';
import { ShoppingCartService } from '../../services/shopping-cart.service';

@Component({
  selector: 'product-quantity',
  templateUrl: './product-quantity.component.html',
  styleUrls: ['./product-quantity.component.css']
})
export class ProductQuantityComponent {

  @Input() product: Product;
  @Input() shoppingCart: ShoppingCart;

  constructor(private cartService: ShoppingCartService) { }
  
  get quantity() {
    return this.shoppingCart.getQuantity(this.product.id);
  }

  addToCart() {
    this.cartService.addToCart(this.product.id).subscribe( () => {
      this.shoppingCart.updateQuantity(this.product, 1);
    })
  }

  removeFromCart() {
    this.cartService.removeFromCart(this.product).subscribe( () => {
      this.shoppingCart.updateQuantity(this.product, -1);
    })
  }

}
