import { JsonPipe } from '@angular/common';
import { ErrorHandler, Inject, Injectable, Injector } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AppErrorHandler implements ErrorHandler {

  constructor(@Inject(Injector) private injector: Injector) {
    // super();
  }

  private get toastrService(): ToastrService {
    return this.injector.get(ToastrService);
  }

  handleError(error: Response) {
    if (error.status == 500)
        this.toastrService.error("Internal server error.", "Code: 500");
    else if (error.status == 404)
        this.toastrService.error("Not found.", "Code: 404");
    else if (error.status == 400)
        this.toastrService.error("Bad request.", "Code: 400");
    else
        this.toastrService.error("An unexpected error has occurred.");
    
    console.error('catched error: ' + error);
    console.error('catched error: ' + new JsonPipe().transform(error));
  }
}
